import csv
import sys

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D

from BinarySearchTree import BinarySearchTree
from Point import Point
from Segment import Segment


def initialize_data(status):
	points = []
	main_point = None
	segments = []
	input_file = sys.argv[1]
	first_line = True
	with open(input_file) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')
		try:
			for row in csv_reader:
				if first_line:
					main_point = Point(float(row[0]), float(row[1]))
					first_line = False
				elif len(row) != 4:
					continue
				else:
					p1 = Point(float(row[0]), float(row[1]), main_point)
					p2 = Point(float(row[2]), float(row[3]), main_point)
					points.append(p1)
					points.append(p2)

					s = Segment(p1, p2, main_point)
					segments.append(s)
					if initial_sweep_line_crosses(s, main_point):
						status.put(s.distance, s)
		except ValueError:
			print("Wrong input data format. Abort...")
			exit()
	return segments, points, main_point


def plot(segments, color):
	for segment in segments:
		x = [segment.start.x, segment.end.x]
		y = [segment.start.y, segment.end.y]
		ax.add_line(Line2D(x, y, marker=".", color=color))


def show(segments, main_point):
	plot(segments, "black")
	ax.plot(main_point.x, main_point.y, 'ro')


def initial_sweep_line_crosses(seg, main_point):
	return min(seg.start.y, seg.end.y) < main_point.y <= max(seg.start.y, seg.end.y) and main_point.x < (
		(main_point.y - seg.b) / seg.a)


def add_legend():
	legend_elements = [Line2D([0], [0], marker='o', color='w', label='Main point', markerfacecolor='r', markersize=8),
					   Line2D([0], [0], color='black', lw=1, label='Segments not seen from the main point'),
					   Line2D([0], [0], color='r', lw=1, label='Segments seen from the main point')]
	ax.legend(handles=legend_elements, loc="upper center", bbox_to_anchor=(0.5, 1.18))


def prepare_report(report):
	result_csv = "result.csv"
	with open(result_csv, 'w') as csv_output:
		print("Segments seen from " + str(main_point))
		csv_output.write(str(main_point) + "\n")
		for seg in report:
			csv_output.write(str(seg) + "\n")
			print(seg)
		print("Visible segments has been saved to", result_csv)


def save_plot_to_pdf():
	filename = "result.pdf"
	pdf = PdfPages(filename)
	pdf.savefig(fig)
	pdf.close()
	print("Plot has been saved in", filename)


if __name__ == '__main__':
	fig, ax = plt.subplots()
	status = BinarySearchTree()
	segments, points, main_point = initialize_data(status)

	report = set([])
	points.sort(key=lambda p: (p.angle, p.order_key(main_point)))
	for point in points:
		if status.get(point.line.distance) is None:
			status.put(point.line.distance, point.line)
		else:
			status.delete(point.line.distance)

		if status.size > 0:
			report.add(status.find_min().payload)

	prepare_report(report)

	show(segments, main_point)
	add_legend()
	plot(report, "red")
	plt.grid()
	plt.plot()
	save_plot_to_pdf()
	plt.show()
