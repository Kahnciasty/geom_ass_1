## Manual  
In order to run the script please open a terminal in the location of all script files. Execute command:
```bash 
python3 main.py input_file.csv
```
### Input format  
The script is prepared to read `.csv` files with comma as a delimiter. Example:
```csv
10, 10
18, 12,   19, 8
19, 12.5, 20, 8
16, 12,   16, 9.8
12, 5,    17, 9
```

First line of an input file contains two coordinates `(x, y)` of the main point. Following lines contain information about line segments. Every line in the input file symbolizes new segment. Example of a line segment notation:

```
x1, y1, x2, y2
```

where `endpoint1: x1, y1` and `endpoint2: x2, y2`.
All the numbers in a file are considered floating point numbers, however integers do not have to be followed by `.0`.

### Output format  
After executing the script user is able to see plot with all the line segments and the main point. The plot is saved to `result.pdf` file and stored in the same directory as the script files. List of all the segments that are visible from the main point is also saved to `result.csv` file in the same format and location as input data.

