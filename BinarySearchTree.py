from TreeNode import TreeNode


class BinarySearchTree:
	def __init__(self):
		self.root = None
		self.size = 0

	def length(self):
		return self.size

	def __len__(self):
		return self.size

	def put(self, key, val):
		if self.root:
			self._put(key, val, self.root)
		else:
			self.root = TreeNode(key, val)
		self.size = self.size + 1

	def _put(self, key, val, current_node):
		if key < current_node.key:
			if current_node.has_left_child():
				self._put(key, val, current_node.leftChild)
			else:
				current_node.leftChild = TreeNode(key, val, parent=current_node)
		else:
			if current_node.has_right_child():
				self._put(key, val, current_node.rightChild)
			else:
				current_node.rightChild = TreeNode(key, val, parent=current_node)

	def __setitem__(self, k, v):
		self.put(k, v)

	def get(self, key):
		if self.root:
			res = self._get(key, self.root)
			if res:
				return res.payload
			else:
				return None
		else:
			return None

	def _get(self, key, current_node):
		if not current_node:
			return None
		elif current_node.key == key:
			return current_node
		elif key < current_node.key:
			return self._get(key, current_node.leftChild)
		else:
			return self._get(key, current_node.rightChild)

	def __getitem__(self, key):
		return self.get(key)

	def __contains__(self, key):
		if self._get(key, self.root):
			return True
		else:
			return False

	def delete(self, key):
		if self.size > 1:
			node_to_remove = self._get(key, self.root)
			if node_to_remove:
				self.remove(node_to_remove)
				self.size = self.size - 1
			else:
				raise KeyError('Key not found')
		elif self.size == 1 and self.root.key == key:
			self.root = None
			self.size = self.size - 1
		else:
			raise KeyError('Key not found')

	def __delitem__(self, key):
		self.delete(key)

	def find_min(self):
		current = self.root
		if current is None:
			return None
		while current.has_left_child():
			current = current.leftChild
		return current

	def remove(self, current_node):
		if current_node.is_leaf():
			if current_node == current_node.parent.leftChild:
				current_node.parent.leftChild = None
			else:
				current_node.parent.rightChild = None
		elif current_node.has_both_children():
			succ = current_node.find_successor()
			succ.splice_out()
			current_node.key = succ.key
			current_node.payload = succ.payload

		else:
			if current_node.has_left_child():
				if current_node.is_left_child():
					current_node.leftChild.parent = current_node.parent
					current_node.parent.leftChild = current_node.leftChild
				elif current_node.is_right_child():
					current_node.leftChild.parent = current_node.parent
					current_node.parent.rightChild = current_node.leftChild
				else:
					current_node.replace_node_data(current_node.leftChild.key,
												   current_node.leftChild.payload,
												   current_node.leftChild.leftChild,
												   current_node.leftChild.rightChild)
			else:
				if current_node.is_left_child():
					current_node.rightChild.parent = current_node.parent
					current_node.parent.leftChild = current_node.rightChild
				elif current_node.is_right_child():
					current_node.rightChild.parent = current_node.parent
					current_node.parent.rightChild = current_node.rightChild
				else:
					current_node.replace_node_data(current_node.rightChild.key,
												   current_node.rightChild.payload,
												   current_node.rightChild.leftChild,
												   current_node.rightChild.rightChild)
