from Point import Point


class Segment:
	def __init__(self, p1, p2, point):
		if p1.x == p2.x:
			self.start = max(p1, p2, key=lambda p: p.y)
			self.end = min(p1, p2, key=lambda p: p.y)
		else:
			self.start = min(p1, p2, key=lambda p: p.x)
			self.end = max(p1, p2, key=lambda p: p.x)

		p1.line = self
		p2.line = self
		self.a, self.b = self.calculate_segments_a_and_b()
		self.distance = self.calculate_distance_to(point)

	def calculate_distance_to(self, point):

		a_perp = -1 / self.a
		b_perp = point.y - a_perp * point.x

		if self.a == a_perp:
			a_prim = 0.0001
		else:
			a_prim = self.a - a_perp

		x_intersect = (b_perp - self.b) / a_prim
		y_intersect = self.a * x_intersect + self.b

		if self.start.x < x_intersect < self.end.x:
			ret_dist = point.calculate_distance_to(Point(x_intersect, y_intersect))
			return ret_dist

		dist1 = point.calculate_distance_to(self.start)
		dist2 = point.calculate_distance_to(self.end)
		return min(dist1, dist2)

	def calculate_segments_a_and_b(self):
		if self.start.x == self.end.x:
			x_zero = 0.0001
		else:
			x_zero = self.start.x - self.end.x

		a = (self.start.y - self.end.y) / x_zero
		if a == 0:
			a = 0.0001
		b = self.start.y - a * self.start.x
		return a, b

	def initial_sweep_line_crosses_segment(self, main_point):
		return min(self.start.y, self.end.y) < main_point.y <= max(self.start.y, self.end.y) and main_point.x < (
			(main_point.y - self.b) / self.a)

	def __str__(self):
		return str(str(self.start.x) + ", " + str(self.start.y) + ", " + str(self.end.x) + ", " + str(
			self.end.y))
