import math


class Point:
	def __init__(self, x=0, y=0, p1=None):
		self.x = x
		self.y = y
		if p1 is not None:
			self.angle = self.calculate_angle_to_x_axis(p1)
		else:
			self.angle = 0

	def calculate_angle_to_x_axis(self, p1):
		a = math.degrees(math.atan2(self.y - p1.y, self.x - p1.x))
		if a < 0:
			a += 360
		return a

	def calculate_distance_to(self, point):
		return math.sqrt(pow(point.x - self.x, 2) + pow(point.y - self.y, 2))

	def order_key(self, main_point):
		dist = self.calculate_distance_to(main_point)
		if self.sweep_line_exits_segment(main_point):
			return -dist
		return dist

	def sweep_line_exits_segment(self, main_point):
		return (self.line.start == self and self.line.end.angle < self.angle) or (
			self.line.end == self and self.line.start.angle < self.angle) or (
				   self.line.start == self and self.line.end.angle > self.angle and self.line.initial_sweep_line_crosses_segment(
					   main_point)) or (
				   self.line.end == self and self.line.start.angle > self.angle and self.line.initial_sweep_line_crosses_segment(
					   main_point))

	def __str__(self):
		return str(str(self.x) + ", " + str(self.y))
